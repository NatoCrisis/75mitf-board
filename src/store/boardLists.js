import moment from "moment";
const array = require('lodash/array');
export default {
    namespaced: true,
    state() {
        return {
            lists: [
                { id: 12, listTitle: 'List 1', listItems: [
                        {id: 1, title: 'List item 1', description: 'huzza blah',
                            labels: [11],
                            attachments: [],
                            checklists: [
                                {
                                    id: 21,
                                    name: 'my checklist',
                                    items: [ { id: 49, label: 'do this', value: false }, { id: 50, label: 'and do this', value: true } ]
                                },
                                {
                                    id: 212,
                                    name: 'another checklist',
                                    items: [ { id: 494, label: 'do this', value: false }, { id: 505, label: 'and do this', value: true } ]
                                }
                            ],
                            },
                        {id: 2, title: 'List item 2',description: 'something',
                            labels: [],
                            attachments: [],
                            checklists: [
                                {
                                    id: 22,
                                    name: 'my checklist',
                                    items: [ { id: 55, label: 'do this', value: false }, { id: 56, label: 'and do this', value: true } ]
                                }
                            ],
                            }] },

                { id: 18, listTitle: 'List 2', listItems: [
                        {id: 3, title: 'List item 3', description: null, labels: [], checklist:{}} ] }
            ]
        }
    },
    mutations: {
        addList(state, payload) {
            const idx = Math.floor(Math.random() * 1000);
            let newList = { id: idx,
                listTitle: payload.listName,
                listItems: [],
                labels: [],
                checkList: null
            };

            state.lists.push(newList);
        },
        removeList(state, payload) {
            const idx = state.lists.findIndex((l) => l.id === payload.listId);
            state.lists.splice(idx, 1);
        },
        updateListTitle(state, payload) {
          state.lists.find((l) => l.id === payload.listId).listTitle = payload.listTitle;
        },
        updateListItemTitle(state, payload) {
            state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .title = payload.title;
        },
        addListItem(state, payload) {

            // TODO: remove this cloneing
            let firstItem = state.lists[0].listItems[0];
            let clone = { ...firstItem};

            clone.id = Math.floor(Math.random() * 1000);
            clone.title = payload.itemName;
            console.log(clone);
            state.lists.find((l) => l.id === payload.listId)
                .listItems.push(clone);
        },
        deleteListItem(state, payload) {
            // console.log(state.lists);
            const list = state.lists.find((l) => l.id === payload.listId);
            const idx = list.listItems.findIndex((l) => l.id === payload.listItemId);
            list.listItems.splice(idx, 1);
            // console.log(state.lists);
        },
        updateChecklistItem(state, payload) {
            // console.log(payload)
            // console.log(state.lists.find((l) => l.id === payload.listId));
            state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .checklists.find(c => c.id === payload.checklistId)
                .items.find(i => i.id === payload.itemId).value = payload.value;
        },
        addChecklistItem(state, payload) {
            let newItem = {};
            newItem.id = Math.floor(Math.random() * 1000);
            newItem.label = payload.name;
            newItem.value = false;
            state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .checklists.find(c => c.id === payload.checklistId)
                .items.push(newItem);
        },
        deleteChecklistItem(state, payload) {
            const idx = state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .checklists.find(c => c.id === payload.checklistId)
                .items.findIndex(i => i.id === payload.itemId);

            state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .checklists.find(c => c.id === payload.checklistId)
                .items.splice(idx, 1);
        },
        addChecklist(state, payload) {

            const idx = Math.floor(Math.random() * 1000);
            let newChecklist = {
                id: idx,
                name: payload.checklistName,
                items: []
            };

            state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .checklists.push(newChecklist);

        },
        deleteChecklist(state, payload) {
            const idx = state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .checklists.findIndex(c => c.id === payload.checklistId);

            state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .checklists.splice(idx, 1);
        },
        addAttachment(state, payload) {
            const attachment = {
                id: Math.floor(Math.random() * 1000),
                name: payload.name,
                uploadDate: moment()
            };

            state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .attachments.push(attachment);
        },
        deleteAttachment(state, payload) {
            array.remove(
            state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .attachments, function(a) {
                    return payload.ids.includes(a.id)
                });
        },
        addLabel(state, payload) {
            state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .labels.push(payload.id);
        },
        deleteLabel(state, payload) {
            array.remove(
                state.lists.find((l) => l.id === payload.listId)
                    .listItems.find(li => li.id === payload.listItemId)
                    .labels, function(a) {
                        return payload.labelId === a
                    });
        },
        updateDescription(state, payload) {
            console.log(payload.description);
            state.lists.find((l) => l.id === payload.listId)
                .listItems.find(li => li.id === payload.listItemId)
                .description = payload.description;
        }
    },
    actions: {
        addList({ commit }, payload) {
            commit('addList', payload);
        },
        removeList({ commit }, payload) {
            commit('removeList', payload);
        },
        updateListTitle({ commit }, payload) {
            commit('updateListTitle', payload);
        },
        updateListItemTitle({ commit }, payload) {
            commit('updateListItemTitle', payload);
        },
        addListItem({ commit }, payload) {
            commit('addListItem', payload);
        },
        deleteListItem({ commit }, payload) {
            commit('deleteListItem', payload);
        },
        updateChecklistItem( { commit }, payload) {
            commit('updateChecklistItem', payload);
        },
        addChecklistItem( { commit }, payload) {
            commit('addChecklistItem', payload);
        },
        deleteChecklistItem( { commit }, payload) {
            commit('deleteChecklistItem', payload);
        },
        addChecklist({ commit }, payload) {
          commit('addChecklist', payload);
        },
        deleteChecklist( { commit }, payload) {
            commit('deleteChecklist', payload);
        },
        addAttachment( { commit }, payload) {
            commit('addAttachment', payload);
        },
        deleteAttachment({ commit }, payload) {
            commit('deleteAttachment', payload);
        },
        addLabel({ commit }, payload) {
          commit('addLabel', payload);
        },
        deleteLabel({ commit }, payload) {
            commit('deleteLabel', payload);
        },
        updateDescription({ commit }, payload) {
            commit('updateDescription', payload);
        }

    },
    getters: {
        lists(state) {
            return state.lists;
        },
        listItems: (state) => (id) => {
            return state.lists.find(l => l.id === id).listItems;
        },
        listTitle: (state) => (id) => {
            return state.lists.find(l => l.id === id).listTitle;
        },
        getTitle: (state) => (listId, listItemId) => {
            return state.lists.find(l => l.id === listId)
                .listItems.find(li => li.id === listItemId).title;
        },
        getLabels: (state) => (listId, listItemId) => {
            return state.lists.find(l => l.id === listId)
                .listItems.find(li => li.id === listItemId).labels;
        },
        getChecklist: (state) => (listId, listItemId, checklistId) => {
            return state
                .lists.find((l) => l.id === listId)
                .listItems.find(li => li.id === listItemId)
                .checklists.find(c => c.id === checklistId)
        },
        getDescription: (state) => (listId, listItemId) => {
            return state
                .lists.find((l) => l.id === listId)
                .listItems.find(li => li.id === listItemId).description;
        }

    }
}
