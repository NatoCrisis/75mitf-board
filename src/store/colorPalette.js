export default {
    namespaced: true,
    state() {
        return {
            colors: [
                { color: 'red', qname: 'red', tcolor: 'white'},
                { color: 'pink', qname: 'pink', tcolor: 'white'},
                { color: 'purple', qname: 'purple', tcolor: 'white'},
                { color: 'indigo', qname: 'indigo', tcolor: 'white'},
                { color: 'blue', qname: 'blue', tcolor: 'white'},
                { color: 'teal', qname: 'teal', tcolor: 'white'},
                { color: 'green', qname: 'green-14', tcolor: 'black'},
                { color: 'yellow', qname: 'yellow-12', tcolor: 'black'},
                { color: 'orange', qname: 'orange-10', tcolor: 'white'},
                { color: 'brown', qname: 'brown-6', tcolor: 'white'},
                { color: 'grey', qname: 'grey', tcolor: 'white'}
            ]
        }

    },
    getters: {
        getAllColors(state) {
            return state.colors;
        },
        getAllColorNames(state) {
            return state.colors.map(c => c.color);
        },
        getColor: (state) => (name) => {
            return state.colors.find(c => c.color === name);
        }

    }
}
