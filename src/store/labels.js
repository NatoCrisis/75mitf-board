export default {
    namespaced: true,
    state() {
        return {
            labels: [
                { id: 11, color: 'red-6', title: 'Important'},
                { id: 12, color: 'purple-6', title: ''},
                { id: 13, color: 'blue-10', title: ''},
                { id: 14, color: 'green', title: ''},
                { id: 15, color: 'yellow', title: ''},
                { id: 16, color: 'amber-10', title: ''},
                { id: 17, color: 'grey-7', title: ''},
                { id: 18, color: 'blue-grey-6', title: ''}

            ]
        }
    },
    mutations: {
        updateLabelTitle(state, payload) {
            state.labels.find(s => s.id === payload.id)
                .title = payload.title;
        }
    },
    actions: {
        updateLabelTitle( { commit }, payload) {
            commit('updateLabelTitle', payload);
        }
    },
    getters: {
        getAllLabels(state) {
            return state.labels;
        },
        getAllColors(state) {
          return state.labels.map(c => c.color);
        },
        getLabelById: (state) => (id) => {
            return state.labels.find(s => s.id === id)
        },
        getLabelByTitle: (state) => (title) => {
            return state.labels.find(s => s.title.toLowerCase() === title.toLowerCase())
        },
        findLabels: (state) => (idList) => {
            return state.labels.filter(l => idList.includes(l.id));
        }
    }
}
