import Vuex from 'vuex'
import Vue from 'vue';
import boardLists from "@/store/boardLists";
import labels from "@/store/labels";
import colors from "@/store/colorPalette";

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        boardLists: boardLists,
        labels: labels,
        colors: colors
    }
});
export default store;
